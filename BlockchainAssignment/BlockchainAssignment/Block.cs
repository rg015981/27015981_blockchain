﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BlockchainAssignment
{
    class Block
    {
        public int blockLevel;
        public long blockTime;

        /* Block Variables */
        private DateTime listTimestamp; // Time of creation

        private int index, // Position of the block in the sequence of blocks
            difficulty = 4; // An arbitrary number of 0's to proceed a hash value

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress; // Public Key (Wallet Address) of the Miner

        public List<Transaction> transactionList; // List of transactions in this block

        // Proof-of-work
        public long nonce; // Number used once for Proof-of-Work and mining

        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"

        /* Genesis block constructor */
        public Block()
        {
            listTimestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = Mine();
            blockLevel = Blockchain.difficulty;
        }

        /* New Block constructor */
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress)
        {
            blockLevel = Blockchain.difficulty;

            listTimestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions

            if ((BlockchainApp.retrieveDiff() == true) && (Blockchain.listTimes.Count % 3 == 0))
            {
                alterDiff();
            }

            hash = Mine(); // Conduct PoW to create a hash which meets the given difficulty requirement

        }

        public void alterDiff()
        {
            double avgTime = Blockchain.listTimes.Average(); //the average time of needed to create the block
            long initialTime = Blockchain.listTimes.First(); // the target time it should of achieved

            long bound_Low = (long)(initialTime * 0.90); //how much lower than the inital time would be before it would increase/decrease difficulty
            long bound_High = (long)(initialTime * 1.10);// how much higher than the inital time it should be before it should increase/decrease difficulty
            if (avgTime < bound_Low)//if the average time is lower than the lower bound
            {
                Blockchain.difficulty += 1; // change diffiulty to 1 higher
                blockLevel += 1; //increase difficulty by 1

                Console.WriteLine("Average Time: " + avgTime); //display avg time
                Console.WriteLine("Target: " + initialTime); //display inital time
                Console.WriteLine("New Difficulty: " + blockLevel); //display new difficulty
                Blockchain.listTimes.Clear();
            }
            if ((avgTime > bound_High) && (Blockchain.difficulty != 1)) //if averagetime is quicker than inital then
            {
                Blockchain.difficulty -= 1; // decrease difficulty
                blockLevel -= 1; //decrease difficulty

                Console.WriteLine("Average Time: " + avgTime); //average time displayed
                Console.WriteLine("Target: " + initialTime); // intital time
                Console.WriteLine("New Difficulty: " + blockLevel); //new difficulty found
                Blockchain.listTimes.Clear(); // clear list times
            }
        }

        /* Hashes the entire Block object */
        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = listTimestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }

        // Create a Hash which satisfies the difficulty level required for PoW
        public String Mine()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string hash = "";
            string diffString = new string('0', Blockchain.difficulty);
            while (hash.StartsWith(diffString) == false)
            {
                hash = this.CreateHash();
                this.nonce++;
            }
            this.nonce--;

            blockTime = sw.ElapsedMilliseconds;
            Blockchain.listTimes.Add(blockTime);

            return hash; // Return the hash meeting the difficulty requirement
        }

        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"

            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i = 0; i < hashes.Count; i += 2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tlistTimestamp: " + listTimestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + blockLevel
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                + "\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }
    }
}
