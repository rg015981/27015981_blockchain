﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        // Global blockchain object
        private Blockchain blockchain;

        public static bool levelCheck;

        // Default App Constructor
        public BlockchainApp()
        {
            // Initialise UI Components
            InitializeComponent();
            // Create a new blockchain 
            blockchain = new Blockchain();
            // Update UI with an initalisation message
            UpdateText("New blockchain initialised!");
        }
        public static bool retrieveDiff()
        {
            return levelCheck;
        }

        /* PRINTING */
        // Helper method to update the UI with a provided message
        private void UpdateText(String text)
        {
            output.Text = text;
        }

        // Print entire blockchain to UI
        private void ReadAll_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.ToString());
        }

        // Print Block N (based on user input)
        private void PrintBlock_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(blockNo.Text, out int index))
                UpdateText(blockchain.GetBlockAsString(index));
            else
                UpdateText("Invalid Block No.");
        }

        // Print pending transactions from the transaction pool to the UI
        private void PrintPendingTransactions_Click(object sender, EventArgs e)
        {
            if (blockchain.transactionPool.Count != 0 && radioButton1.Checked == true) // checks if radiobox is ticked and if transaction pool > 0
            {
                blockchain.transactionPool.Sort((y, x) => x.listTimestamp.CompareTo(y.listTimestamp)); // compares y,x against x y and swaps x and y so shows the newest ones first
                UpdateText(String.Join("\n", blockchain.transactionPool)); // updates transaction pools
            }

            else if (blockchain.transactionPool.Count != 0 && Greedy.Checked == true) // checks if greedy is ticked and if transaction pool > 0
            {
                blockchain.transactionPool.Sort((y, x) => x.fee.CompareTo(y.fee)); // compares y,x against x y and swaps x and y so shows the highest fees transactions first
                UpdateText(String.Join("\n", blockchain.transactionPool)); // updates transaction pools

            }

            else if (blockchain.transactionPool.Count != 0 && Unpredictable.Checked == true) // checks if unpredictable is ticked and if transaction pool > 0
            {
                Random unPred = new Random(); //creates new random intitator
                int index = blockchain.transactionPool.Count; // counts how many blocks there are
                while (index > 1) // if index is greater than 1
                {
                    index--; // removes index
                    int index2 = unPred.Next(index + 1); //index of second randomised + 1 
                    Transaction number = blockchain.transactionPool[index2]; //a new transaction is placed at the index of randomised
                    blockchain.transactionPool[index2] = blockchain.transactionPool[index]; // the second index is assigned to the assigned index
                    blockchain.transactionPool[index] = number; // index = new number 
                    UpdateText(String.Join("\n", blockchain.transactionPool)); // updates transactions pools

                }
            }


            else if (blockchain.transactionPool.Count != 0 && altruistic.Checked == true) // checks if altrustic is ticked and if transaction pool > 0
            {
                blockchain.transactionPool.Sort((x, y) => x.listTimestamp.CompareTo(y.listTimestamp)); // compares x,y against x y and swaps x and y so shows the newest ones first
                UpdateText(String.Join("\n", blockchain.transactionPool));// updates transactions pools
            }
            else if (blockchain.transactionPool.Count != 0 && Address.Checked == true)  // checks if address is ticked and if transaction pool > 0
            {
                List<Transaction> Address = new List<Transaction>(); // create new list to store the transaction for address
                foreach (Transaction transaction in blockchain.transactionPool) // for each aspect in transaction pool
                {
                    string address = textBox1.Text; //address is equal to the text box
                    if (address.Equals(transaction.senderAddress)) // if the transaction of the entered key is equal to the sender address
                    {
                        Address.Add(transaction); // puts the transaction to the list
                    }
                }
                UpdateText(String.Join("\n",Address)); //updates all transaction pools

            }


        }


        /* WALLETS */
        // Generate a new Wallet and fill the public and private key fields of the UI
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out string privKey);

            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validate the keys loaded in the UI by comparing their mathematical relationship
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
                UpdateText("Keys are valid");
            else
                UpdateText("Keys are invalid");
        }

        // Check the balance of current user
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.GetBalance(publicKey.Text).ToString() + " Assignment Coin");
        }


        /* TRANSACTION MANAGEMENT */
        // Create a new pending transaction and add it to the transaction pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, reciever.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
            /* TODO: Validate transaction */
            blockchain.transactionPool.Add(transaction);
            UpdateText(transaction.ToString());
        }

        /* BLOCK MANAGEMENT */
        // Conduct Proof-of-work in order to mine transactions from the pool and submit a new block to the Blockchain
        private void NewBlock_Click(object sender, EventArgs e)
        {
            // Retrieve pending transactions to be added to the newly generated Block
            List<Transaction> transactions = blockchain.GetPendingTransactions();

            // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
            blockchain.blocks.Add(newBlock);

            UpdateText(blockchain.ToString());
        }


        /* BLOCKCHAIN VALIDATION */
        // Validate the integrity of the state of the Blockchain
        private void Validate_Click(object sender, EventArgs e)
        {
            // CASE: Genesis Block - Check only hash as no transactions are currently present
            if(blockchain.blocks.Count == 1)
            {
                if (!Blockchain.ValidateHash(blockchain.blocks[0])) // Recompute Hash to check validity
                    UpdateText("Blockchain is invalid");
                else
                    UpdateText("Blockchain is valid");
                return;
            }

            for (int i=1; i<blockchain.blocks.Count-1; i++)
            {
                if(
                    blockchain.blocks[i].prevHash != blockchain.blocks[i - 1].hash || // Check hash "chain"
                    !Blockchain.ValidateHash(blockchain.blocks[i]) ||  // Check each blocks hash
                    !Blockchain.ValidateMerkleRoot(blockchain.blocks[i]) // Check transaction integrity using Merkle Root
                )
                {
                    UpdateText("Blockchain is invalid");
                    return;
                }
            }
            UpdateText("Blockchain is valid");
        }

        private void BlockchainApp_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                levelCheck = true;
            }
            else
            {
                levelCheck = false;
            }
        }
    }
}